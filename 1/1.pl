#!/usr/bin/env perl
# solution to advent of code 2023 day 1
# https://adventofcode.com/2023/day/1
# JBL 2023-12-20


use v5.38.0;

my $file = './input';
my $numbers = {
    "zero" => 0,
    "one" => 1,
    "two" => 2,
    "three" => 3,
    "four" => 4,
    "five" => 5,
    "six" => 6,
    "seven" => 7,
    "eight" => 8,
    "nine" => 9,
    # "0" => 0,
    # "1" => 1,
    # "2" => 2,
    # "3" => 3,
    # "4" => 4,
    # "5" => 5,
    # "6" => 6,
    # "7" => 7,
    # "8" => 8,
    # "9" => 9,
    };

my $renumbers = join '|', map quotemeta, keys %$numbers;
open my $input, $file or die "could not open $file: $!";

my $sum = 0;
while ( my $line = <$input> ) {
    print $line;
    $line =~ m/((\d)|$renumbers).*/;
    my ($firstnum) = ($1);
    if (exists $numbers->{$firstnum}) {
        $firstnum = $numbers->{$firstnum};
    };

    $line =~ m/.*((\d)|$renumbers)/;
    my ($lastnum) = ($1);
    if (exists $numbers->{$lastnum}) {
        $lastnum = $numbers->{$lastnum};
    };

    say "first digit = $firstnum";
    say "last digit = $lastnum";
    my $number = $firstnum . $lastnum ;
    say "number: $number";
    $sum += $number;
#    last if $. == 6;
}
        close $input;
say "Final sum: $sum";
